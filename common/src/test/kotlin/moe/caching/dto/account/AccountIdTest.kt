package moe.caching.dto.account

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import java.lang.IllegalArgumentException
import java.util.UUID

internal class AccountIdTest {
    @ParameterizedTest
    @ValueSource(strings = ["", " ", "Test 123", "a/b", "account-123123", "account*123123"])
    fun failsOnInvalidValue(invalidValue: String) {
        Assertions.assertThatThrownBy { AccountId.fromDescriptiveName(invalidValue) }
            .isInstanceOf(IllegalArgumentException::class.java)
    }

    @ParameterizedTest
    @ValueSource(strings = ["account*4a7f7064-3784-4b14-9938-8f04c81d690f", "account*cbb9ec97-4ed6-4112-90f0-2ff47861c372"])
    fun succeedsOnValidValue(validValue: String) {
        AccountId.fromDescriptiveName(validValue)
    }

    @Test
    fun descriptiveFormatMatches() {
        val uuid = UUID.fromString("4a7f7064-3784-4b14-9938-8f04c81d690f")
        Assertions.assertThat(AccountId(uuid).descriptiveName()).isEqualTo("account*4a7f7064-3784-4b14-9938-8f04c81d690f")
    }
}