package moe.caching.dto.account

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue
import java.util.UUID

data class AccountId(val id: UUID) {
    @JsonValue
    fun descriptiveName() = DESCRIPTIVE_PREFIX + id


    companion object {
        const val DESCRIPTIVE_PREFIX = "account*"

        @JvmStatic
        @JsonCreator
        fun fromDescriptiveName(descriptiveName: String): AccountId {
            if (!descriptiveName.startsWith(DESCRIPTIVE_PREFIX)) {
                throw IllegalArgumentException("expected prefix to be $DESCRIPTIVE_PREFIX")
            }

            return AccountId(UUID.fromString(descriptiveName.removePrefix(DESCRIPTIVE_PREFIX)))
        }
    }
}