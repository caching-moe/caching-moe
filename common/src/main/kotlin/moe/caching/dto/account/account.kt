package moe.caching.dto.account

import javax.validation.constraints.NotBlank

data class CreateAccountData(
    @field:NotBlank
    val name: String,
)

data class UpdateAccountData(
    @field:NotBlank
    val name: String?,
)

data class Account(
    val id: AccountId,
    val name: String,
)
