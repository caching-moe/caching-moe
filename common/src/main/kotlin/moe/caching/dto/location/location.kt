package moe.caching.dto.location

data class Location(
    val id: LocationId,
)