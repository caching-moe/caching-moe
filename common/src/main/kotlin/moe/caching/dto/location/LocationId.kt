package moe.caching.dto.location

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue
import javax.validation.constraints.Size

data class LocationId(@field:Size(min = 1, max = 16) val id: String) {
    @JsonValue
    fun descriptiveName() = DESCRIPTIVE_PREFIX + id
    override fun toString() = descriptiveName()

    companion object {
        const val DESCRIPTIVE_PREFIX = "location*"

        @JvmStatic
        @JsonCreator
        fun fromDescriptiveName(descriptiveName: String): LocationId {
            if (!descriptiveName.startsWith(DESCRIPTIVE_PREFIX)) {
                throw IllegalArgumentException("expected prefix to be $DESCRIPTIVE_PREFIX")
            }

            return LocationId(descriptiveName.removePrefix(DESCRIPTIVE_PREFIX))
        }
    }
}