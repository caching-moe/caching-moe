package moe.caching.dto.node

import java.net.URL

fun Node.fullUrl(networkBase: URL)  =
    URL(networkBase.protocol, "${this.subdomain.subdomain}.${networkBase.host}", networkBase.file)

