package moe.caching.dto.node

import moe.caching.dto.account.AccountId
import moe.caching.dto.location.LocationId
import java.time.OffsetDateTime

data class CreateNodeData(
    val ownerId: AccountId,
    val locationId: LocationId,
)

data class UpdateNodeData(
    val locationId: LocationId?,
    val ipv4: IpPort?,

    val buildVersion: Int?,
    val diskBytes: Long?,
    val speedBytes: Long?,

    val lastPing: OffsetDateTime?,
)

data class IpPort(
    val ip: String,
    val port: Int,
)

data class Node(
    val id: NodeId,
    val ownerId: AccountId,
    val locationId: LocationId,
    val secret: String,
    val subdomain: Subdomain,

    val diskBytes: Long,
    val speedBytes: Long,

    val paused: Boolean,
    val turbo: Boolean,
)

data class Subdomain(
    val subdomain: String,
    val tlsCert: TlsCert,
    val ipPort: IpPort?,
)

data class TlsCert(
    val issuedAt: OffsetDateTime,
    val privateKey: ByteArray,
    val certificate: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TlsCert

        if (issuedAt != other.issuedAt) return false
        if (!privateKey.contentEquals(other.privateKey)) return false
        if (!certificate.contentEquals(other.certificate)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = issuedAt.hashCode()
        result = 31 * result + privateKey.contentHashCode()
        result = 31 * result + certificate.contentHashCode()
        return result
    }
}
