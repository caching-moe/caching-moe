package moe.caching.dto.node

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue
import java.util.UUID

data class NodeId(val id: UUID) {
    @JsonValue
    fun descriptiveName() = DESCRIPTIVE_PREFIX + id
    override fun toString() = descriptiveName()

    companion object {
        const val DESCRIPTIVE_PREFIX = "node*"

        @JvmStatic
        @JsonCreator
        fun fromDescriptiveName(descriptiveName: String): NodeId {
            if (!descriptiveName.startsWith(DESCRIPTIVE_PREFIX)) {
                throw IllegalArgumentException("expected prefix to be $DESCRIPTIVE_PREFIX, was $descriptiveName")
            }

            return NodeId(UUID.fromString(descriptiveName.removePrefix(DESCRIPTIVE_PREFIX)))
        }
    }
}