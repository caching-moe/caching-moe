package moe.caching.agent.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.validation.annotation.Validated
import java.net.URL
import javax.validation.constraints.NotNull

/**
 * Properties used by the agent to communicate with the control API
 */
@Validated
@ConstructorBinding
@ConfigurationProperties("caching-moe.agent.control-api")
class ControlApiSettings(
    /**
     * URL used to communicate with the control API
     */
    @field:NotNull
    val url: URL
)