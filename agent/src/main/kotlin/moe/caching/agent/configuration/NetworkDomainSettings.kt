package moe.caching.agent.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.validation.annotation.Validated
import java.net.URL
import javax.validation.constraints.NotNull


@Validated
@ConstructorBinding
@ConfigurationProperties("caching-moe.agent.network")
class NetworkDomainSettings(
    /**
     * URL used as base url for resolving nodes
     */
    @field:NotNull
    val networkBase: URL
)