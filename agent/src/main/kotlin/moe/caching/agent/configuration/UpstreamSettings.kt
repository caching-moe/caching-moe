package moe.caching.agent.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.validation.annotation.Validated
import java.net.URL
import javax.validation.constraints.NotNull


@Validated
@ConstructorBinding
@ConfigurationProperties("caching-moe.agent.upstream")
class UpstreamSettings(
    /**
     * Server for upstream fetches from caches
     */
    @field:NotNull
    val upstream: URL,
    /**
     * Server for fallback fetches when caches are not avaliable
     */
    @field:NotNull
    val fallback: URL
)