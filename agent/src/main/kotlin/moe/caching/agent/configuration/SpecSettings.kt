package moe.caching.agent.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.Min

@Validated
@ConfigurationProperties("caching-moe.agent.node-api.spec")
class SpecSettings(
    /**
     * Oldest supported spec version
     */
    @field:Min(0)
    val min: Int = 0,
    /**
     * Latest supported spec version
     */
    @field:Min(0)
    val max: Int = 0
)