package moe.caching.agent.api.v1

import moe.caching.agent.configuration.NetworkDomainSettings
import moe.caching.agent.configuration.SpecSettings
import moe.caching.agent.service.node.NodeService
import moe.caching.agent.service.token.KeyService
import moe.caching.agent.service.upstream.UpstreamService
import moe.caching.dto.node.IpPort
import moe.caching.dto.node.UpdateNodeData
import moe.caching.dto.node.fullUrl
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.time.OffsetDateTime

@RestController
class NodeController(
    private val specSettings: SpecSettings,
    private val nodeService: NodeService,
    private val upstreamService: UpstreamService,
    private val keyService: KeyService,
    private val networkDomainSettings: NetworkDomainSettings
) : ApiV1 {
    @PostMapping("/ping")
    fun ping(@RequestBody pingRequest: PingRequest): PingResponse {
        val node = nodeService.ping(
            pingRequest.secret,
            UpdateNodeData(
                ipv4 = IpPort(
                    ip = "192.168.1.1",
                    port = pingRequest.port,
                ),
                speedBytes = pingRequest.networkSpeedBytesPerSecond,
                diskBytes = pingRequest.diskSpaceBytes,
                buildVersion = pingRequest.specVersion,
                lastPing = OffsetDateTime.now(),
                // TODO: is this needed
                locationId = null,
            )
        )

        return PingResponse(
            latestBuild = specSettings.max,
            isPaused = node.paused,
            isCompromised = false, // TODO: Wat do
            clientId = node.id,
            tls = TlsCert(
                createdAt = node.subdomain.tlsCert.issuedAt,
                certificate = String(node.subdomain.tlsCert.certificate),
                privateKey = String(node.subdomain.tlsCert.privateKey),
            ),
            publicKey = keyService.publicPemEncoded,
            clientUrl = node.fullUrl(networkDomainSettings.networkBase),
            imageServer = upstreamService.serverForUpstream(),
        )
    }

    @PostMapping("/stop")
    fun stop(@RequestBody stopRequest: StopRequest) {
        nodeService.stop(stopRequest.secret)
    }
}