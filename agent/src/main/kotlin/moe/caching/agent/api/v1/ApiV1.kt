package moe.caching.agent.api.v1

import org.springframework.web.bind.annotation.RequestMapping

/**
 * Backwards-compatible with legacy server API
 */
@RequestMapping("/")
interface ApiV1