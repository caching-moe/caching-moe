package moe.caching.agent.api.v1

import com.fasterxml.jackson.annotation.JsonProperty
import moe.caching.dto.node.NodeId
import java.net.URL
import java.time.OffsetDateTime
import java.util.Optional
import java.util.UUID
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class PingRequest(
    val secret: @Size(max = 52, min = 52) String,
    val ipAddress: Optional<String>,
    @field:JsonProperty("port")
    val port: @Min(0) Int,
    @field:JsonProperty("disk_space")
    val diskSpaceBytes: @Min(0) Long,
    @field:JsonProperty("network_speed")
    val networkSpeedBytesPerSecond: @Min(0) Long,
    @field:JsonProperty("build_version")
    val specVersion: @Min(value = 0L) Int,
)

data class TlsCert(
    val createdAt: OffsetDateTime,
    val certificate: String,
    val privateKey: String,
)

data class PingResponse(
    val imageServer: URL,
    val latestBuild: Int,
    @field:JsonProperty("url")
    val clientUrl: URL,
    val publicKey: String,
    @field:JsonProperty("compromised")
    val isCompromised: Boolean,
    @field:JsonProperty("paused")
    val isPaused: Boolean,
    val clientId: NodeId,
    val tls: TlsCert?,
)

data class StopRequest(
    @field:NotNull
    @field:Size(max = 52, min = 52)
    val secret: String
)