package moe.caching.agent.api.internal

import moe.caching.agent.service.report.ReportService
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestBody
import java.net.URL

data class ReportRequest(
    val url: URL,
    val success: Boolean,
    val bytes: Int,
    val duration: Double, // backwards compat, should be integral
    val cached: Boolean? = null, // unused but backwards compat
)

@RestController
class ReportController(private val reportService: ReportService) {
    @PostMapping("/report")
    @CrossOrigin
    fun report(@RequestBody request: ReportRequest) {
        println(request.url)
    }
}