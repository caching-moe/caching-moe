package moe.caching.agent.api.internal

import moe.caching.agent.service.assign.AssignService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.net.URL

data class AssignRequest(
    val chapterHash: String,
    val images: List<String>,
    val is443: Boolean,
)

data class AssignResponse(
    val server: URL,
)

@RestController
class AssignController(private val assignService: AssignService) {
    @PostMapping("/assign")
    fun assign(@RequestBody request: AssignRequest): AssignResponse =
        AssignResponse(
            assignService.assignNode(request.chapterHash, request.images, request.is443)
        )
}