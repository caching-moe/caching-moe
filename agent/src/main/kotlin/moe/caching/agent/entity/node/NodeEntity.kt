package moe.caching.agent.entity.node

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.BitSet
import java.util.UUID

@Document
data class NodeEntity(
    @field:Id
    val id: UUID,
    val assignedShards: BooleanArray = BooleanArray(65536),
    val successShortTerm: Double = 0.70,
    val successLongTerm: Double = 0.70,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as NodeEntity

        if (id != other.id) return false
        if (!assignedShards.contentEquals(other.assignedShards)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + assignedShards.contentHashCode()
        return result
    }

    companion object {
        const val SUCCESS_SHORT_DECAY = 0.01;
        const val SUCCESS_LONG_DECAY = 0.0005;
    }
}