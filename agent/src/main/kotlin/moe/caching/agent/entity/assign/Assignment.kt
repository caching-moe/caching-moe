package moe.caching.agent.entity.assign

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant
import java.util.UUID

@Document
data class Assignment(
    @field:Id
    val id: UUID,
    val nodeId: UUID,
    @Indexed(expireAfter = "15m")
    val time: Instant,

    @Indexed(unique = true)
    val token: String,
    val pages: List<String>,
)