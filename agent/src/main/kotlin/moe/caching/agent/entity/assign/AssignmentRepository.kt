package moe.caching.agent.entity.assign

import org.springframework.data.mongodb.repository.MongoRepository
import java.time.Instant
import java.util.Optional
import java.util.UUID

interface AssignmentRepository: MongoRepository<Assignment, UUID> {
    fun countByNodeIdAndTimeAfter(nodeId: UUID, time: Instant): Long
    fun findByToken(token: String): Optional<Assignment>
}
