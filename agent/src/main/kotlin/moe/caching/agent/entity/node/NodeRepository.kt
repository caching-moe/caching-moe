package moe.caching.agent.entity.node

import org.springframework.data.mongodb.repository.MongoRepository
import java.util.UUID

interface NodeRepository: MongoRepository<NodeEntity, UUID>