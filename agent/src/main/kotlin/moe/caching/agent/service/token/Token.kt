package moe.caching.agent.service.token

import java.time.OffsetDateTime

data class Token(
    val expires: OffsetDateTime
)