package moe.caching.agent.service.node.scoring

import moe.caching.agent.entity.assign.AssignmentRepository
import moe.caching.agent.entity.node.NodeRepository
import moe.caching.agent.service.shard.ShardService.Companion.SHARD_COUNT
import moe.caching.dto.node.NodeId
import org.springframework.stereotype.Service
import java.time.Instant
import kotlin.math.log10
import kotlin.math.pow

const val TARGET_SPEED = 1200000 // 1.2MB/s

@Service
class ScoringService(private val nodeRepository: NodeRepository, private val assignmentRepository: AssignmentRepository) {
    fun scoreNode(id: NodeId, turbo: Boolean, networkSpeed: Long, speedLimiter: Long = 0): Double {
        val node = nodeRepository.findById(id.id).orElseThrow()
        val assigned = assignmentRepository.countByNodeIdAndTimeAfter(id.id, Instant.now().minusSeconds(30))

        // if speedLimiter is defined, make sure
        // the speed cannot exceed it
        val speed = run {
            if (speedLimiter > 0) {
                networkSpeed.coerceAtMost(speedLimiter)
            } else {
                networkSpeed
            }
        }

        // subtract amount of speed intended for currently active assignments
        val userSpeed = (speed - assigned * TARGET_SPEED).coerceAtLeast(0)

        val coercedSuccessShort = node.successShortTerm.coerceAtMost(0.99)
        val coercedSuccessLong = node.successLongTerm.coerceAtMost(0.9999)

        val failRate = (1 - coercedSuccessShort) * (1 - coercedSuccessLong).pow(2)

        // try to balance requests so having 1 shard is as good as having many.
        // this will fail really badly in a situation with few nodes, but we
        // have to take care of shard allocation first before we can really fix that
        val curShards = node.assignedShards.fold(0) { num, v -> if (v) num + 1 else num }
        val shardWeighting = SHARD_COUNT.toDouble() / curShards.toDouble()

        var baseSpeed = userSpeed / failRate * shardWeighting.pow(2)
        if (turbo) {
            baseSpeed *= 1e9;
        }

        return log10(baseSpeed) // make the assignments a little bit more "fair"
    }
}