package moe.caching.agent.service.token

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import moe.caching.dto.node.NodeId
import org.bouncycastle.openssl.jcajce.JcaPEMWriter
import org.springframework.stereotype.Service
import java.io.StringWriter
import java.security.*
import java.security.spec.ECGenParameterSpec
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.util.Base64


@Service
class KeyService {
    private val keyPair: KeyPair

    init {
        val g = KeyPairGenerator.getInstance("EC")
        g.initialize(ECGenParameterSpec("secp256r1"), SecureRandom())
        keyPair = g.generateKeyPair()
    }

    val public: PublicKey
        get() = keyPair.public

    val publicPemEncoded: String by lazy {
        StringWriter().use {
            JcaPEMWriter(it).use { pem ->
                pem.writeObject(public)
            }
            it.toString()
        }
    }

    val private: PrivateKey
        get() = keyPair.private
}

@Service
class TokenService(private val keyService: KeyService) {
    private val mapper = JsonMapper.builder()
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        .addModule(JavaTimeModule())
        .build()

    private val encoder = Base64.getUrlEncoder().withoutPadding()

    fun signToken(clientId: NodeId, chapterHash: String): String {
        val token = Token(OffsetDateTime.now(ZoneOffset.UTC).plusMinutes(15))
        val msg = mapper.writeValueAsBytes(token)

        val ecdsaSign = Signature.getInstance("SHA256withECDSA")
        ecdsaSign.initSign(keyService.private)
        ecdsaSign.update(clientId.toString().toByteArray())
        ecdsaSign.update(chapterHash.toByteArray())

        val sig = ecdsaSign.sign()
        return "${encoder.encodeToString(msg)}=${encoder.encodeToString(sig)}"
    }
}