package moe.caching.agent.service.node

import moe.caching.agent.entity.node.NodeEntity
import moe.caching.agent.entity.node.NodeEntity.Companion.SUCCESS_LONG_DECAY
import moe.caching.agent.entity.node.NodeEntity.Companion.SUCCESS_SHORT_DECAY
import moe.caching.agent.entity.node.NodeRepository
import moe.caching.agent.service.control.ControlService
import moe.caching.agent.service.shard.ShardService
import moe.caching.dto.node.Node
import moe.caching.dto.node.NodeId
import moe.caching.dto.node.UpdateNodeData
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.lang.IllegalArgumentException
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.read
import kotlin.concurrent.write

const val BYTES_PER_SHARD = 80000000 // 80 MB

@Service
class NodeService(
    private val controlService: ControlService,
    private val nodeRepository: NodeRepository,
    private val shardService: ShardService
) {
    val lastPingMap = ConcurrentHashMap<NodeId, Instant>()

    // reading in this case means non destructive updates (pings)
    // writing is reserved for the case of removing a node
    val readWriteLock = ReentrantReadWriteLock()

    // this expects lastPingMap to not have an entry for `nodeId`
    // if it has not pinged previously in a 2 minute period
    private fun updateNodeData(nodeId: NodeId, shardsToAssign: Int) {
        if (shardsToAssign > ShardService.SHARD_COUNT) {
            throw IllegalArgumentException("too many shards")
        }

        val nodeOpt = nodeRepository.findById(nodeId.id)
        // get or save the node entity to mongo
        val node = run {
            if (nodeOpt.isEmpty) {
                nodeRepository.save(
                    NodeEntity(
                        id = nodeId.id,
                        assignedShards = BooleanArray(65536),
                    )
                )
            } else {
                val mongoNode = nodeOpt.get()

                if (!lastPingMap.containsKey(nodeId)) {
                    mongoNode.assignedShards.forEachIndexed { shardId, assigned ->
                        if (assigned) {
                            shardService.addNode(shardId, nodeId)
                        }
                    }
                }
                mongoNode
            }
        }

        val curShards = node.assignedShards.fold(0) { num, assigned -> if (assigned) num + 1 else num }
        // see whether we need to remove or add more shards
        // to match the number of shards that we are assigned
        if (curShards < shardsToAssign) {
            // this could be some weighting that also takes into
            // account inactive nodes, but seems extraneous for now
            val shardSizes = shardService.sortedSizes()
            var counter = 0
            for (i in shardSizes.asReversed()) {
                if (!node.assignedShards[i.id]) {
                    node.assignedShards[i.id] = true
                    shardService.addNode(i.id, NodeId(node.id))

                    if (++counter == shardsToAssign - curShards) {
                        break;
                    }
                }
            }
        } else if (curShards > shardsToAssign) {
            val shardSizes = shardService.sortedSizes()
            var counter = 0
            for (i in shardSizes) {
                if (node.assignedShards[i.id]) {
                    node.assignedShards[i.id] = false
                    shardService.removeNode(i.id, NodeId(node.id))

                    if (++counter == shardsToAssign - curShards) {
                        break;
                    }
                }
            }
        }

        nodeRepository.save(node)
    }

    // TODO: have custom DTO here instead of [UpdateNodeData]?
    fun ping(secret: String, updateNodeData: UpdateNodeData): Node {
        val oldNode = controlService.getBySecret(secret)
        // do excessive node ping things here
        val node = controlService.update(oldNode.id, updateNodeData)
//        if (node.locationId != LocationId("test")) {
//            throw IllegalArgumentException("location id mismatch")
//        }
        readWriteLock.read {
            updateNodeData(node.id, (node.diskBytes / BYTES_PER_SHARD).coerceAtMost(ShardService.SHARD_COUNT.toLong()).toInt())
            lastPingMap[node.id] = updateNodeData.lastPing!!.toInstant()
        }

        return node
    }

    fun stop(secret: String) {
        val node = controlService.getBySecret(secret);
        val mongoNode = nodeRepository.findById(node.id.id).orElseThrow()

        readWriteLock.write {
            mongoNode.assignedShards.forEachIndexed { shardId, assigned ->
                if (assigned) {
                    shardService.removeNode(shardId, node.id)
                }
            }

            lastPingMap.remove(node.id)
        }
    }

    @Scheduled(fixedDelay = 20000)
    private fun clearExpiredNodes() = readWriteLock.write {
        val now = Instant.now()
        val entrySet = lastPingMap.entries

        with(entrySet.iterator()) {
            while (hasNext()) {
                val (id, time) = next()
                if (now.isAfter(time.plusSeconds(PING_MAX_VALIDITY_SECONDS))) {
                    val node = nodeRepository.findById(id.id).orElseThrow()
                    node.assignedShards.forEachIndexed { shardId, assigned ->
                        if (assigned) {
                            shardService.removeNode(shardId, id)
                        }
                    }
                    remove()
                }
            }
        }
    }

    @Transactional
    fun updateSuccess(id: NodeId, successful: Boolean) {
        val success = if (successful) 1.0 else 0.0
        val node = nodeRepository.findById(id.id).orElseThrow()

        nodeRepository.save(
            node.copy(
                successLongTerm = success * SUCCESS_LONG_DECAY + node.successLongTerm * (1 - SUCCESS_LONG_DECAY),
                successShortTerm = success * SUCCESS_SHORT_DECAY + node.successShortTerm * (1 - SUCCESS_SHORT_DECAY),
            )
        )
    }

    companion object {
        const val PING_MAX_VALIDITY_SECONDS = 120L
    }
}