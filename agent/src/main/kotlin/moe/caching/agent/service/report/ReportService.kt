package moe.caching.agent.service.report

import moe.caching.agent.entity.assign.AssignmentRepository
import moe.caching.agent.service.node.NodeService
import moe.caching.dto.node.NodeId
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Duration

@Service
class ReportService(private val nodeService: NodeService, private val assignmentRepository: AssignmentRepository) {
    @Transactional
    fun report(token: String, page: String, successful: Boolean, bytes: Int, duration: Duration): Boolean {
        val assignmentOpt = assignmentRepository.findByToken(token)

        if (assignmentOpt.isEmpty) {
            return false
        }

        val assignment = assignmentOpt.get()

        if(!assignment.pages.contains(page)) {
            return false
        }

        assignmentRepository.save(
            assignment.copy(
                pages = assignment.pages - token
            )
        )
        nodeService.updateSuccess(NodeId(assignment.id), successful)

        return true
    }
}