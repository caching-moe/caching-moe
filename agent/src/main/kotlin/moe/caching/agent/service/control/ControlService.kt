package moe.caching.agent.service.control

import moe.caching.agent.configuration.ControlApiSettings
import moe.caching.dto.node.*
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.stereotype.Service
import org.springframework.web.util.UriComponentsBuilder

@Service
class ControlService(controlApiSettings: ControlApiSettings, restTemplateBuilder: RestTemplateBuilder) {
    private val restTemplate = restTemplateBuilder.rootUri(controlApiSettings.url.toExternalForm()).build()

    fun getBySecret(secret: String): Node {
        return restTemplate.getForObject(
            "/internal/node/by-secret/{secret}",
            Node::class.java,
            secret
        )!!
    }

    fun getAllByIds(ids: List<NodeId>): List<Node> {
        val uriString = UriComponentsBuilder
            .fromUriString("/internal/node/by-ids")
            .queryParam("ids", ids)
            .build().toUriString()

        return restTemplate.getForObject(
            uriString,
            Array<Node>::class.java,
        )!!.asList()
    }

    fun update(nodeId: NodeId, request: UpdateNodeData): Node {
        return restTemplate.postForObject(
            "/internal/node/{node-id}",
            request,
            Node::class.java,
            nodeId
        )!!
    }
}