package moe.caching.agent.service.upstream

import moe.caching.agent.configuration.UpstreamSettings
import org.springframework.stereotype.Service
import java.net.URL

@Service
class UpstreamService(private val upstreamSettings: UpstreamSettings) {
    fun serverForUpstream(): URL = upstreamSettings.upstream
    fun serverForFallback(): URL = upstreamSettings.fallback
}