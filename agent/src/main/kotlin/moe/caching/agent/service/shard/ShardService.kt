package moe.caching.agent.service.shard

import moe.caching.dto.node.NodeId
import org.springframework.stereotype.Service
import java.util.concurrent.ConcurrentHashMap

private class Shard {
    val nodes: MutableSet<NodeId> = ConcurrentHashMap.newKeySet()
}

data class ShardCount(
    val id: Int,
    val count: Int,
)

/**
 * This service controls the in memory storage of shards
 * to nodes. This should be synced with the persistence layer.
 */
@Service
class ShardService {
    private val shards = List(SHARD_COUNT) { Shard() }

    /**
     * Estimates the counts of each shard and returns
     * them sorted in descending order by count
     *
     * This method is safe to call concurrently but does
     * not represent a snapshot at any one point in time,
     * yet is adequate for estimation purposes.
     *
     * @return a list of shard ids sorted by count
     */
    fun sortedSizes(): List<ShardCount> =
        shards.asSequence().mapIndexed { index, it -> ShardCount(index, it.nodes.size) }
            .sortedByDescending { it.count }.toList()

    /**
     * Get the nodes currently in the shard. This method is safe to call concurrently,
     * but may not reflect some updates.
     *
     * @return a list of ids of the nodes currently in the shard
     */
    fun nodes(id: Int): List<NodeId> {
        return shards[id].nodes.toList()
    }

    /**
     * Adds the specified [NodeId] to the shard. This method is safe to call concurrently.
     *
     * @return `true` if the id has been added, `false` if it is already present.
     */
    fun addNode(id: Int, node: NodeId): Boolean {
        return shards[id].nodes.add(node)
    }

    /**
     * Removes the specified [NodeId] from the shard. This method is safe to call concurrently.
     *
     * @return ``true` if the id has been removed, `false` if it was not present.`
     */
    fun removeNode(id: Int, node: NodeId): Boolean {
        return shards[id].nodes.remove(node)
    }

    companion object {
        /**
         * The number of shards we are assuming
         */
        const val SHARD_COUNT = 65536
    }
}