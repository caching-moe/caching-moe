package moe.caching.agent.service.assign

import moe.caching.agent.configuration.NetworkDomainSettings
import moe.caching.agent.entity.assign.Assignment
import moe.caching.agent.entity.assign.AssignmentRepository
import moe.caching.agent.service.control.ControlService
import moe.caching.agent.service.node.scoring.ScoringService
import moe.caching.agent.service.shard.ShardService
import moe.caching.agent.service.token.TokenService
import moe.caching.agent.service.upstream.UpstreamService
import moe.caching.dto.node.Node
import moe.caching.dto.node.fullUrl
import org.apache.commons.rng.sampling.DiscreteProbabilityCollectionSampler
import org.apache.commons.rng.simple.RandomSource
import org.apache.commons.rng.simple.ThreadLocalRandomSource
import org.springframework.stereotype.Service
import java.net.URL
import java.time.Instant
import java.util.UUID

@Service
class AssignService(
    private val shardService: ShardService,
    private val controlService: ControlService,
    private val scoringService: ScoringService,
    private val upstreamService: UpstreamService,
    private val tokenService: TokenService,
    private val assignmentRepository: AssignmentRepository,
    private val networkDomainSettings: NetworkDomainSettings
) {
    private fun bestNodeForShard(id: Int, is443: Boolean): Node? {
        val nodes = controlService.getAllByIds(shardService.nodes(id))
            .asSequence()
            .filter { it.subdomain.ipPort != null && !it.paused && it.speedBytes > 0 }
            .filter { !is443 || it.subdomain.ipPort!!.port == 443 }
            .map { it to scoringService.scoreNode(it.id, it.turbo, it.speedBytes) }
            .toMap()

        if (nodes.isEmpty()) {
            return null
        }

        return DiscreteProbabilityCollectionSampler(
            ThreadLocalRandomSource.current(RandomSource.SPLIT_MIX_64),
            nodes
        ).sample()
    }

    fun assignNode(chapterHash: String, pages: List<String>, is443: Boolean): URL {
        val shardNum = chapterHash.substring(0 until 4).toInt(16)
        val node = bestNodeForShard(shardNum, is443)
        val url = if (node == null) {
            upstreamService.serverForFallback()
        } else {
            val token = tokenService.signToken(node.id, chapterHash)

            assignmentRepository.save(
                Assignment(
                    UUID.randomUUID(),
                    node.id.id,
                    Instant.now(),
                    token,
                    pages
                )
            )
            val nodeUrl = node.fullUrl(networkDomainSettings.networkBase)
            URL(nodeUrl.protocol, nodeUrl.host, "/$token")
        }

        return url
    }
}