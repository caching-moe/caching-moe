package moe.caching.control.entity.account

import java.time.OffsetDateTime
import java.util.Objects
import javax.persistence.Embeddable
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Embeddable
class SubdomainEmbed(
    @field:NotEmpty
    var subdomain: String,
    @field:NotEmpty
    var certificate: ByteArray,
    @field:NotEmpty
    var privateKey: ByteArray,
    @field:NotNull
    var issuedAt: OffsetDateTime,
    var ipv4: String? = null,
    @field:Min(0)
    @field:Max(65536)
    var port: Int? = null,
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other !is SubdomainEmbed) {
            return false
        }
        return subdomain == other.subdomain
    }

    override fun hashCode(): Int {
        return Objects.hash(subdomain)
    }
}