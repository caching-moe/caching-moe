package moe.caching.control.entity.location

import java.time.OffsetDateTime
import java.util.Objects
import javax.persistence.*
import javax.validation.constraints.Size

@Entity(name = "Location")
@Table(schema = "control", name = "location")
class LocationEntity(
    @field:Size(min = 1, max = 16)
    @field:Id
    val id: String,
    @field:Version var updatedAt: OffsetDateTime? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other !is LocationEntity) {
            return false
        }
        return id == other.id
    }

    override fun hashCode(): Int {
        return Objects.hash(id)
    }
}