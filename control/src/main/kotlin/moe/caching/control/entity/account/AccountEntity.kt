package moe.caching.control.entity.account

import java.time.OffsetDateTime
import java.util.Objects
import java.util.UUID
import javax.persistence.Id
import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Version
import javax.validation.constraints.NotBlank

@Entity(name = "account")
@Table(schema = "control", name = "account")
class AccountEntity(
    @field:Id val id: UUID,
    @field:NotBlank var name: String,
    @field:Version var updatedAt: OffsetDateTime? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other !is AccountEntity) {
            return false
        }
        return id == other.id
    }

    override fun hashCode(): Int {
        return Objects.hash(id)
    }
}