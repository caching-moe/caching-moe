package moe.caching.control.entity.node

import moe.caching.control.entity.account.AccountEntity
import moe.caching.control.entity.location.LocationEntity
import moe.caching.control.entity.account.SubdomainEmbed
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.OffsetDateTime
import java.util.Objects
import java.util.UUID
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Entity(name = "node")
@Table(schema = "control", name = "node")
class NodeEntity(
    @field:Id val id: UUID,
    @field:NotNull
    @field:ManyToOne(fetch = FetchType.LAZY, optional = false)
    @field:OnDelete(action = OnDeleteAction.CASCADE)
    var owner: AccountEntity,
    @field:NotNull
    @field:ManyToOne(fetch = FetchType.LAZY)
    var location: LocationEntity,
    @field:NotBlank
    @field:Column(unique = true)
    var secret: String,
    var lastPing: OffsetDateTime?,

    @field:Embedded
    var subdomain: SubdomainEmbed,

    @field:NotNull
    var buildVersion: Int,
    @field:NotNull
    var diskBytes: Long,
    @field:NotNull
    var speedBytes: Long,

    @field:NotNull
    var paused: Boolean,
    @field:NotNull
    var turbo: Boolean,
) {
    @field:Version
    var updatedAt: OffsetDateTime? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other !is NodeEntity) {
            return false
        }
        return id == other.id
    }

    override fun hashCode(): Int {
        return Objects.hash(id)
    }
}