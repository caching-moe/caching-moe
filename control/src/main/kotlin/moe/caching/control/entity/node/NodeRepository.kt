package moe.caching.control.entity.node

import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional
import java.util.UUID

interface NodeRepository : JpaRepository<NodeEntity, UUID> {
    fun findBySecret(secret: String): Optional<NodeEntity>
    fun findByOwnerId(ownerId: UUID): List<NodeEntity>
    fun findByLocationId(locationId: String): List<NodeEntity>
}