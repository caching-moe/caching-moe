package moe.caching.control

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class ControlApplication

fun main(args: Array<String>) {
    runApplication<ControlApplication>(*args)
}
