package moe.caching.control.service.account

import moe.caching.control.entity.account.AccountEntity
import moe.caching.control.entity.account.AccountRepository
import moe.caching.dto.account.Account
import moe.caching.dto.account.AccountId
import moe.caching.dto.account.CreateAccountData
import moe.caching.dto.account.UpdateAccountData
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class AccountService(private val accountRepository: AccountRepository) {
    fun create(data: CreateAccountData): Account {
        return accountRepository.save(
            AccountEntity(
                id = UUID.randomUUID(),
                name = data.name,
            )
        ).toApi()
    }

    fun delete(accountId: AccountId) {
        accountRepository.deleteById(accountId.id)
    }

    fun findById(accountId: AccountId): Account {
        return accountRepository.findById(accountId.id)
            // really do this?
            .orElseThrow()
            .toApi()
    }

    fun update(accountId: AccountId, update: UpdateAccountData): Account {
        val entity = accountRepository.findById(accountId.id)
            // really do this?
            .orElseThrow()

        update.name?.let {
            entity.name = it
        }
        return accountRepository.save(entity).toApi()
    }

    private fun AccountEntity.toApi(): Account {
        return Account(AccountId(this.id), this.name)
    }
}