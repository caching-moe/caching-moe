package moe.caching.control.service.location

import moe.caching.control.entity.location.LocationEntity
import moe.caching.dto.location.LocationId
import moe.caching.control.entity.location.LocationRepository
import moe.caching.dto.location.Location
import org.springframework.stereotype.Service

@Service
class LocationService(private val locationRepository: LocationRepository) {
    fun create(id: String): Location {
        val entity = LocationEntity(id)
        return locationRepository.save(entity).toApi()
    }

    fun findById(locationId: LocationId): Location {
        val entity: LocationEntity = locationRepository
            .findById(locationId.id)
            .orElseThrow()
        return entity.toApi()
    }

    fun findAll(): List<Location> {
        return locationRepository.findAll().map { it.toApi() }
    }

    fun delete(locationId: LocationId) {
        locationRepository.deleteById(locationId.id)
    }

    private fun LocationEntity.toApi(): Location {
        return Location(id = LocationId(this.id))
    }
}