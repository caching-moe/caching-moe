package moe.caching.control.service.subdomain

import moe.caching.control.service.certdns.CertdnsService
import moe.caching.dto.node.Subdomain
import moe.caching.dto.node.TlsCert
import org.springframework.stereotype.Service
import java.time.OffsetDateTime
import java.time.ZoneOffset
import javax.transaction.Transactional
import kotlin.math.pow

@Service
@Transactional
class SubdomainService(private val certdnsService: CertdnsService) {
    fun refreshSubdomain(subdomain: String): Subdomain {
        val response = certdnsService.generateCertificate(subdomain)

        return Subdomain(
            subdomain,
            TlsCert(
                OffsetDateTime.now(ZoneOffset.UTC),
                response.privateKey,
                response.certificate
            ),
            null
        )
    }

    fun newSubdomain(ipv4: String? = null): Subdomain {
        val subdomain = generateSubdomain()
        val response = certdnsService.generateCertificate(subdomain)
        if (ipv4 != null) {
            certdnsService.setDnsRecord(subdomain, ipv4)
        }

        return Subdomain(
            subdomain,
            TlsCert(
                OffsetDateTime.now(ZoneOffset.UTC),
                response.privateKey,
                response.certificate
            ),
            null,
        )
    }


    fun updateSubdomainIp(subdomain: String, newIp: String) {
        certdnsService.setDnsRecord(subdomain, newIp)
    }

    fun deleteSubdomain(subdomain: String) {
        certdnsService.clearDnsRecord(subdomain)
    }

    companion object {
        private val CHOICES = (0 until 2.0.pow(48).toLong())
        fun generateSubdomain(): String {
            val rand = CHOICES.random()
            return "%012x".format(rand)
        }
    }
}
