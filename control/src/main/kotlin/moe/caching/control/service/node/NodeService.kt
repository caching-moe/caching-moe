package moe.caching.control.service.node

import moe.caching.control.entity.account.AccountRepository
import moe.caching.dto.location.LocationId
import moe.caching.control.entity.location.LocationRepository
import moe.caching.control.entity.node.NodeEntity
import moe.caching.control.entity.node.NodeRepository
import moe.caching.control.entity.account.SubdomainEmbed
import moe.caching.control.service.subdomain.SubdomainService
import moe.caching.dto.account.AccountId
import moe.caching.dto.node.*
import org.springframework.stereotype.Service
import java.lang.IllegalArgumentException
import java.util.UUID

@Service
class NodeService(
    private val nodeRepository: NodeRepository,
    private val locationRepository: LocationRepository,
    private val accountRepository: AccountRepository,
    private val subdomainService: SubdomainService
) {
    fun findById(nodeId: NodeId): Node {
        return nodeRepository.findById(nodeId.id).orElseThrow().toApi()
    }

    fun findBySecret(secret: String): Node {
        return nodeRepository.findBySecret(secret).orElseThrow().toApi()
    }

    fun findAllById(nodeId: Iterable<NodeId>): List<Node> {
        return nodeRepository
            .findAllById(nodeId.asSequence().map { it.id }.asIterable())
            .map { it.toApi() }
    }


    fun findByOwner(owner: AccountId): List<Node> {
        return nodeRepository
            .findByOwnerId(owner.id)
            .map { it.toApi() }

    }

    fun findByLocation(locationId: LocationId): List<Node> {
        return nodeRepository
            .findByLocationId(locationId.id)
            .map { it.toApi() }
    }

    fun create(request: CreateNodeData): Node {
        val location = locationRepository.getById(request.locationId.id)
        val owner = accountRepository.getById(request.ownerId.id)
        val generatedSubdomain = subdomainService.newSubdomain()

        val node = try {
            nodeRepository.save(
                NodeEntity(
                    id = UUID.randomUUID(),
                    owner = owner,
                    location = location,
                    secret = generateSecret(),
                    lastPing = null,

                    subdomain = SubdomainEmbed(
                        generatedSubdomain.subdomain,
                        generatedSubdomain.tlsCert.certificate,
                        generatedSubdomain.tlsCert.privateKey,
                        generatedSubdomain.tlsCert.issuedAt
                    ),

                    paused = false,
                    turbo = false,

                    buildVersion = 0,
                    diskBytes = 0,
                    speedBytes = 0,
                )
            )
        } catch (e: Exception) {
            subdomainService.deleteSubdomain(generatedSubdomain.subdomain)
            throw e
        }

        return node.toApi()
    }

    fun update(nodeId: NodeId, request: UpdateNodeData): Node {
        val entity = nodeRepository.getById(nodeId.id)

        request.locationId?.let {
            val location = locationRepository.getById(it.id)
            entity.location = location
        }

        request.buildVersion?.let {
            entity.buildVersion = it
        }

        request.diskBytes?.let {
            entity.diskBytes = it
        }

        request.speedBytes?.let {
            entity.speedBytes = it
        }

        request.lastPing?.let {
            entity.lastPing = it
        }

        val reqIpv4 = request.ipv4
        if (reqIpv4 != null) {
            if (entity.subdomain.ipv4 == null) {
                subdomainService.updateSubdomainIp(entity.subdomain.subdomain, reqIpv4.ip)
                entity.subdomain.ipv4 = reqIpv4.ip
                entity.subdomain.port = reqIpv4.port
            } else if (entity.subdomain.ipv4 != reqIpv4.ip) {
                val sd = subdomainService.newSubdomain(reqIpv4.ip)
                val oldSd = entity.subdomain

                entity.subdomain = SubdomainEmbed(
                    sd.subdomain,
                    sd.tlsCert.certificate,
                    sd.tlsCert.privateKey,
                    sd.tlsCert.issuedAt,
                    reqIpv4.ip,
                    reqIpv4.port,
                )

                subdomainService.deleteSubdomain(oldSd.subdomain)
            }
        }

        return nodeRepository.save(entity).toApi()
    }

    fun delete(nodeId: NodeId) {
        nodeRepository.deleteById(nodeId.id)
    }

    companion object {
        private val CHOICES = (0..Long.MAX_VALUE)
        fun generateSecret(): String {
            return buildString {
                for (i in 1..4) {
                    val rand = CHOICES.random()
                    append("%016x".format(rand))
                }
            }
        }

        private fun NodeEntity.toApi(): Node {
            return Node(
                id = NodeId(this.id),
                locationId = LocationId(this.location.id),
                ownerId = AccountId(this.owner.id),
                secret = this.secret,
                subdomain = Subdomain(
                    subdomain = this.subdomain.subdomain,
                    ipPort = run {
                        if (subdomain.ipv4 == null || subdomain.port == null) {
                            null
                        } else {
                            IpPort(subdomain.ipv4!!, subdomain.port!!)
                        }
                    },
                    tlsCert = TlsCert(
                        issuedAt = this.subdomain.issuedAt,
                        privateKey = this.subdomain.privateKey,
                        certificate = this.subdomain.certificate,
                    )
                ),

                diskBytes = this.diskBytes,
                speedBytes = this.speedBytes,

                paused = this.paused,
                turbo = this.turbo,
            )
        }
    }
}