package moe.caching.control.service.certdns

import moe.caching.control.configuration.CertdnsApiSettings
import moe.caching.control.service.subdomain.ClearDnsRequest
import moe.caching.control.service.subdomain.GenCertRequest
import moe.caching.control.service.subdomain.GenCertResponse
import moe.caching.control.service.subdomain.SetDnsRequest
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.stereotype.Service

@Service
class CertdnsService(certdnsApiSettings: CertdnsApiSettings, restTemplateBuilder: RestTemplateBuilder) {
    private val restTemplate = restTemplateBuilder.rootUri(certdnsApiSettings.url.toExternalForm()).build()

    fun generateCertificate(subdomain: String): GenCertResponse {
        return restTemplate.postForObject(
            "/gencert",
            GenCertRequest(subdomain),
            GenCertResponse::class.java
        )!!
    }

    fun setDnsRecord(subdomain: String, ipv4: String) {
        return restTemplate.postForObject(
            "/setdns",
            SetDnsRequest(subdomain, ipv4),
            Unit::class.java
        )!!
    }

    fun clearDnsRecord(subdomain: String) {
        return restTemplate.postForObject(
            "/cleardns",
            ClearDnsRequest(subdomain),
            Unit::class.java
        )!!
    }
}