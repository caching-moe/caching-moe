package moe.caching.control.service.subdomain

data class GenCertRequest(
    val subdomain: String,
)

data class GenCertResponse(
    val privateKey: ByteArray,
    val certificate: ByteArray,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GenCertResponse

        if (!privateKey.contentEquals(other.privateKey)) return false
        if (!certificate.contentEquals(other.certificate)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = privateKey.contentHashCode()
        result = 31 * result + certificate.contentHashCode()
        return result
    }
}

data class SetDnsRequest(
    val subdomain: String,
    val ipv4: String,
)

data class ClearDnsRequest(
    val subdomain: String,
)