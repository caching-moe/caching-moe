package moe.caching.control

import moe.caching.AccountIdConverter
import moe.caching.LocationIdConverter
import moe.caching.NodeIdConverter
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.context.annotation.Configuration
import org.springframework.format.FormatterRegistry
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
@ConfigurationPropertiesScan
@EnableTransactionManagement
class ControlConfiguration: WebMvcConfigurer {
    override fun addFormatters(registry: FormatterRegistry) {
        registry.addConverter(AccountIdConverter())
        registry.addConverter(LocationIdConverter())
        registry.addConverter(NodeIdConverter())
    }
}