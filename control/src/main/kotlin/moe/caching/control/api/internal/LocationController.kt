package moe.caching.control.api.internal

import moe.caching.dto.location.LocationId
import moe.caching.dto.location.Location
import moe.caching.control.service.location.LocationService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/internal/location")
class LocationController(val locationService: LocationService) {
    @GetMapping("")
    fun list(): List<Location> {
        return locationService.findAll()
    }

    @PostMapping("")
    fun create(@RequestBody name: String): Location {
        return locationService.create(name)
    }

    @GetMapping("/{locationId}")
    fun findById(@PathVariable locationId: LocationId): Location {
        return locationService.findById(locationId)
    }

    @DeleteMapping("/{locationId}")
    fun delete(@PathVariable locationId: LocationId) {
        locationService.delete(locationId)
    }
}