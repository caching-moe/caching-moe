package moe.caching.control.api.internal

import moe.caching.control.service.account.AccountService
import moe.caching.dto.account.Account
import moe.caching.dto.account.AccountId
import moe.caching.dto.account.CreateAccountData
import moe.caching.dto.account.UpdateAccountData
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/internal/account")
class AccountController(private val accountService: AccountService) {
    @PostMapping("")
    fun create(@RequestBody createAccountData: CreateAccountData): Account {
        return accountService.create(createAccountData)
    }

    @GetMapping("/{accountId}")
    fun findById(@PathVariable accountId: AccountId): Account {
        return accountService.findById(accountId)
    }

    @PutMapping("/{accountId}")
    fun update(@PathVariable accountId: AccountId, @RequestBody updateAccountData: UpdateAccountData): Account {
        return accountService.update(accountId, updateAccountData)
    }

    @DeleteMapping("/{accountId}")
    fun delete(@PathVariable accountId: AccountId) {
        accountService.delete(accountId)
    }
}