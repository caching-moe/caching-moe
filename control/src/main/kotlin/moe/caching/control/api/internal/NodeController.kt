package moe.caching.control.api.internal

import moe.caching.dto.location.LocationId
import moe.caching.dto.node.NodeId
import moe.caching.dto.node.CreateNodeData
import moe.caching.dto.node.Node
import moe.caching.control.service.node.NodeService
import moe.caching.dto.account.AccountId
import moe.caching.dto.node.UpdateNodeData
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.UUID

@RestController
@RequestMapping("/internal/node")
class NodeController(private val nodeService: NodeService)  {
    @GetMapping("/by-ids")
    fun findNodesByIds(@RequestParam ids: List<NodeId>): List<Node> {
        return nodeService.findAllById(ids)
    }

    @GetMapping("/{nodeId}")
    fun findById(@PathVariable nodeId: NodeId): Node {
        return nodeService.findById(nodeId)
    }

    @GetMapping("/by-secret/{secret}")
    fun findBySecret(@PathVariable secret: String): Node {
        return nodeService.findBySecret(secret)
    }

    @GetMapping("/by-owner/{owner}")
    fun findNodesByOwner(@PathVariable owner: AccountId): List<Node> {
        return nodeService.findByOwner(owner)
    }

    @GetMapping("/by-location/{location}")
    fun findNodesByLocation(@PathVariable location: LocationId): List<Node> {
        return nodeService.findByLocation(location)
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody createNodeRequest: CreateNodeData): Node {
        return nodeService.create(createNodeRequest)
    }

    @PostMapping("/{nodeId}")
    fun update(@PathVariable nodeId: NodeId, @RequestBody updateNodeRequest: UpdateNodeData): Node {
        return nodeService.update(nodeId, updateNodeRequest)
    }

    @DeleteMapping("/{nodeId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable nodeId: NodeId) {
        nodeService.delete(nodeId)
    }
}