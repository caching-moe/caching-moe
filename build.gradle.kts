plugins {
	id("org.springframework.boot") version "2.5.5" apply false
	id("net.afanasev.sekret") version "0.1.0" apply false
	id("io.spring.dependency-management") version "1.0.11.RELEASE" apply false
	id("org.asciidoctor.convert") version "1.5.8" apply false
	kotlin("jvm") version "1.5.21" apply false
	kotlin("plugin.spring") version "1.5.21" apply false
	kotlin("plugin.jpa") version "1.5.21" apply false
	id("com.palantir.git-version") version "0.12.3" apply false
}

subprojects {
	apply(plugin = "com.palantir.git-version")

	val gitVersion: groovy.lang.Closure<String> by extra
	version = System.getenv("VERSION") ?: gitVersion()

	repositories {
		mavenCentral()
	}

	tasks.withType<Test> {
		useJUnitPlatform()
	}
}