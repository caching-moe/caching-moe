package moe.caching.dto.location

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import java.lang.IllegalArgumentException

internal class LocationIdTest {
    @ParameterizedTest
    @ValueSource(strings = ["", " ", "Test 123", "a/b", "location-123123"])
    fun failsOnInvalidValue(invalidValue: String) {
        Assertions.assertThatThrownBy { LocationId.fromDescriptiveName(invalidValue) }
            .isInstanceOf(IllegalArgumentException::class.java)
    }

    @ParameterizedTest
    @ValueSource(strings = ["location*asdadsa", "location*asdasd12312"])
    fun succeedsOnValidValue(validValue: String) {
        LocationId.fromDescriptiveName(validValue)
    }

    @Test
    fun descriptiveFormatMatches() {
        Assertions.assertThat(LocationId("123456").descriptiveName()).isEqualTo("location*123456")
    }
}