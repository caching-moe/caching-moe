package moe.caching

import moe.caching.dto.account.AccountId
import moe.caching.dto.location.LocationId
import moe.caching.dto.node.NodeId
import org.springframework.core.convert.converter.Converter

class AccountIdConverter : Converter<String, AccountId> {
    override fun convert(source: String) = AccountId.fromDescriptiveName(source)
}

class LocationIdConverter : Converter<String, LocationId> {
    override fun convert(source: String) = LocationId.fromDescriptiveName(source)
}

class NodeIdConverter : Converter<String, NodeId> {
    override fun convert(source: String) = NodeId.fromDescriptiveName(source)
}